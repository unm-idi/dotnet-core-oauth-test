using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.WebUtilities;

namespace DotNetCoreAdfsOauthSampleClientApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication("adfs").AddCookie("adfs")
                .AddOAuth("ADFS", "ADFS", o =>
                {
                    o.ClientId = Configuration["Auth:Adfs:ClientId"];
                    o.ClientSecret = "secret";
                    o.CallbackPath = new PathString("/signin-adfs");
                    o.AuthorizationEndpoint = Configuration["Auth:Adfs:AuthorizationEndpoint"];
                    o.TokenEndpoint = Configuration["Auth:Adfs:TokenEndpoint"];
                    o.SignInScheme = "adfs";
                    o.ClaimsIssuer = Configuration["Auth:Adfs:ClaimsIssuer"];
                    o.SaveTokens = true;

                    o.Events = new OAuthEvents
                    {
                        OnRedirectToAuthorizationEndpoint = async context =>
                        {
                            var parameter = new Dictionary<string, string>
                            {
                                ["resource"] = Configuration["Auth:Adfs:RelyingPartyTrustIdentifier"]
                            };
                            var query = QueryHelpers.AddQueryString(context.RedirectUri, parameter);
                            context.Response.Redirect(query);
                        },
                        OnTicketReceived = async context =>
                        {
                            context.Properties.Items.TryGetValue(".Token.access_token", out var value);
                            var token = new JwtSecurityToken(value);
                            var identity = new ClaimsIdentity(token.Claims, context.Options.SignInScheme, "upn",
                                "role");
                            context.Principal = new ClaimsPrincipal(identity);
                        }
                    };
                });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}