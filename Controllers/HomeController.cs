﻿using Microsoft.AspNetCore.Mvc;

namespace DotNetCoreAdfsOauthSampleClientApp.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet("~/")]
        public ActionResult Index() => View();
    }
}